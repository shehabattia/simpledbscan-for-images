#Copyright Shehab Attia November 2018
#A Density Based Scan tool for images.
#Lets you input an nxm image to cluster features.
#Usage: clusters, points, labels = simpleDBSCAN(img, radius = 10, eps = 5)

#Import libraries
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import time
from skimage import color

#Implementing image DBScan
timekeeper_dictionary = {}
def timekeeper(message = None):
    global timekeeper_dictionary
    if message is None:
        raise Exception("Timekeeper: you must specify a message")
    if message in timekeeper_dictionary:
        print("Took ",(time.time() - timekeeper_dictionary[message])," seconds to run ", message) #Output computing time
        del timekeeper_dictionary[message] #Delete the message
    else:
        timekeeper_dictionary[message] = time.time() #Start tracking time

def getDistance(x,y):
    return np.sqrt(x**2 + y**2)
def getNeighboringPoints(points, point, radius):
    x_points = points[:,0] - point[0]
    y_points = points[:,1] - point[1]
    distances = getDistance(x_points,y_points)
    distances = distances.reshape(-1,)
    return np.argwhere((distances<=radius) & (distances>=-radius))#Return indecies that are within acceptable radius.
def computeXYZ(image):
    points = []
    timekeeper("computeXYZ") #Start tracking time
    for row in range(0,image.shape[0]):
        for col in range(0,image.shape[1]):
            points.append((row,col,image[row,col])) #Create a nxn matrix of points for easier computation
    timekeeper("computeXYZ") #Output computing time
    return np.array(points)
def getNeighborsWithinThreshold(points, point, threshold):
    intensity = points[:,2]
    intensity = intensity.reshape(-1,)
    print("Looking for color:",point[2])
    return np.argwhere((intensity<=(threshold+point[2])) & (intensity>=(-threshold+point[2]))) #Return indecies that are within threshold.
# Usage: clusters, points, labels = simpleDBSCAN(img, radius = 10, eps = 5)
def simpleDBSCAN(image, radius = 10, threshold = 5, max_clusters = 1000):
    timekeeper("simpleDBSCAN") #Start tracking time
    rows,cols = image.shape
    points = computeXYZ(image) #We'll be using indecies of points from now on.
    clusters = [] #Indecies of points in clusters
    labels = np.zeros((rows*cols,1))-1 #Keep track of indecies we're clustering
    while (len(labels[labels<0])>0):
        #display results
        print(labels.reshape((rows,cols)))
        plt.imshow(labels.reshape(rows,cols),cmap='jet')
        plt.show()
        if (len(clusters)>max_clusters):
            raise Exception("Max cluster limit reached")
        unclustered_indecies = np.argwhere(labels<0)[:,0]
        point_index = unclustered_indecies[0]
        print("Number of clusters:",len(clusters), "Current index",point_index, "Points left:", len(unclustered_indecies))
        point = points[point_index]
        neighbors = getNeighboringPoints(points, point, radius) #Find neighbors within radius
        if (len(neighbors)==0):
            clusters.append([point_index]) #No neighbors, start new cluster
            labels[point_index] = len(clusters)
            continue #Move on to the next point
        print("Neighbors in vicinity:",len(neighbors))
        neighbors = np.intersect1d(getNeighborsWithinThreshold(points, point, threshold), neighbors)
        print("Neighbors within threshold:",len(neighbors))
        if (len(neighbors)==0):
            clusters.append([point_index]) #No neighbors, start new cluster
            labels[point_index] = len(clusters)
            continue #Move on to the next point
        #We have neighbors, find the cluster each point belongs to by scoring.
        scores = np.zeros(len(clusters))
        unclustered_neighbors = []
        for cluster_ind in range(0,len(clusters)):
            cluster = clusters[cluster_ind]
            for neighbor in neighbors:
                if  neighbor in cluster:
                    scores[cluster_ind] = scores[cluster_ind] + 1
                elif labels[neighbor]<0:
                    unclustered_neighbors.append(neighbor)
        if len(scores)==0 or (np.max(scores)==0):
            clusters.append([point_index]) #No neighbors, start new cluster
            labels[point_index] = len(clusters)-1
            print("No neighbors")
        else:
            cluster_ind = np.argmax(scores)
            clusters[cluster_ind].append(point_index)
            labels[point_index] = cluster_ind
            #add neighbors too
            for neighbor in unclustered_neighbors:
                clusters[cluster_ind].append(neighbor)
                labels[neighbor] = cluster_ind
            print("Added to cluster ", np.argmax(scores))

    plt.imshow(labels.reshape(rows,cols),cmap='jet')
    plt.show()
    timekeeper("simpleDBSCAN") #Output time for dbscan to run
    return clusters, points, labels

#Test on the blobs dataset
from sklearn.datasets.samples_generator import make_blobs
X, y = make_blobs(n_samples=5, centers=3, n_features=5)
# plt.scatter(X[:,0],X[:,1],c=y)

img = np.zeros(X.shape[0]*X.shape[0]).reshape(X.shape[0],X.shape[0])
for row in range(0,len(X)):
    for col in range(0,len(X)):
        img[row,col] = y[col]*10

plt.imshow(img)
plt.show()
clusters, points,labels = simpleDBSCAN(img, 100, 2)
print(labels.reshape(img.shape))
print(img.reshape(img.shape))
