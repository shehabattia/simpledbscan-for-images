# A Density Based Scan tool for images.
## Lets you input an nxm image or numpy array to cluster features.
### Usage: clusters, points, labels = simpleDBSCAN(img, radius = 10, eps = 5)